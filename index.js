const express = require('express')
const bodyParser = require('body-parser')  // built-in middleware
const app = express()
const port = 3000
const frontUrl = 'http://localhost:8080'  // url of front-end
const db = require('./queries')

//CORS middleware
const allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', frontUrl)
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  res.header('Access-Control-Allow-Headers', 'Content-Type')

  next()
}

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)
app.use(allowCrossDomain)

// route to look for a GET request on the root (/) URL that returns some JSON
app.get('/', (request, response) => {
  response.json({ info: 'Node.js, Express, and Postgres API' })
})

// app will listen on the port set
app.listen(port, () => {
  console.log(`App running on port ${port}.`)
})

// tells node to exit when user uses Ctrl+C in docker container
process.on('SIGINT', function() {
  process.exit()
})

const currRouter = express.Router()
const cmodRouter = express.Router({ mergeParams: true })
const cdisRouter = express.Router({ mergeParams: true })

currRouter.use('/:triId/trilhas', cmodRouter)
cmodRouter.use('/:modId/modulos', cdisRouter)

currRouter.route('/').get(db.getCurriculos)
currRouter.route('/:id/trilhas').get(db.getTrilhasFromCurriculo)
cmodRouter.route('/:id/modulos').get(db.getModulosFromTrilha)
cdisRouter.route('/:id/disciplinas').get(db.getDisciplinasFromModulo)

app.use('/curriculos', currRouter)

const disRouter = express.Router()

disRouter.route('/').get(db.getDisciplinas)
disRouter.route('/:id/').get(db.getDisciplina)

app.use('/disciplinas', disRouter)

app.post('/login/entrar', db.checkCredentials)
app.post('/login/cadastro', db.addUser)
app.post('/login/cadastro/aluno', db.addAlunoType)
// app.get('/usuarios/:id', db.getUserType)
// app.get('/administrador/:id', db.getAdminInfo)
