const { mapQuery, maQueryReturn, mpQueryReturn, returnError } = require('./helpers')


/*
 * Funções do perfil 'Visitante'
 */

const addAlunoType = (request, response) => {
  const { id, nusp, nome, instituto, email, cpf, nascimento, admissao, endLogradouro,
    endNumero, endComplemento, endCEP, curso } = request.body
  
  maQueryReturn('usuario_existe', id, true).then(res => {
    if (res)
      maQueryReturn('insere_us_pf', [email, 'Aluno'], true).then(res => {
        if (res) mpQueryReturn('insere_aluno', [nusp, nome, instituto, email, cpf, nascimento,
          admissao, endLogradouro, endNumero, endComplemento, endCEP, curso], true).then(res => {
            mapQuery(response, 'insere_pe_us', [res.id_pessoa, id], true)
          }).catch(err => returnError(response, err.message))
      }).catch(err => returnError(response, err.message))
  }).catch(err => returnError(response, err.message))
}


module.exports = {
  addAlunoType
}
