const { maQuery } = require('./helpers')


/*
 * Funções do perfil 'Visitante'
 */

const checkCredentials = (request, response) => {
  const { email, password } = request.body

  maQuery(response, 'checa_credenciais', [email,
    { value: password, isString: true }
  ], true)
}

const addUser = (request, response) => {
  const { email, password } = request.body

  maQuery(response, 'insere_usuario', [email,
    { value: password, isString: true }
  ], true)
}


module.exports = {
  checkCredentials,
  addUser
}