const { mcQuery } = require('./helpers')


/*
 * Funções do perfil 'Visitante'
 */

const getCurriculos = (request, response) => {
  mcQuery(response, 'curriculos_do_curso', 'Ciência da Computação')
}

const getTrilhasFromCurriculo = (request, response) => {
  const { id } = request.params
  mcQuery(response, 'trilhas_do_curriculo', id)
}

const getModulosFromTrilha = (request, response) => {
  const { id } = request.params
  mcQuery(response, 'modulos_da_trilha', id)
}

const getDisciplinasFromModulo = (request, response) => {
  const { id } = request.params

  mcQuery(response, 'disciplinas_do_modulo', id)
}

const getDisciplinas = (request, response) => {
  mcQuery(response, 'disciplinas')
}

const getDisciplina = (request, response) => {
  const { id } = request.params
  mcQuery(response, 'disciplina', id)
}


module.exports = {
  getCurriculos,
  getTrilhasFromCurriculo,
  getModulosFromTrilha,
  getDisciplinasFromModulo,
  getDisciplinas,
  getDisciplina
}