/*
 * Connection to PostgreSQL
 */

const Pool = require('pg').Pool

const host = 'localhost'
const user = 'dba'
const password = 'dba1234'
const port = 5432

const ma = new Pool({ host, user, password, port, database: 'explorausp_ma'})
const mc = new Pool({ host, user, password, port, database: 'explorausp_mc'})
const mp = new Pool({ host, user, password, port, database: 'explorausp_mp'})
const map = new Pool({ host, user, password, port, database: 'explorausp_map'})
const mpc = new Pool({ host, user, password, port, database: 'explorausp_mpc'})

const SQLQuery = (fn, params) => {
  if (!params) return `SELECT * FROM ${fn}()`
  if (!isNaN(params)) return `SELECT * FROM ${fn}(${params})`
  if (typeof params === 'string') return `SELECT * FROM ${fn}('${params}')`

  let query = typeof params[0] === 'string'
    ? `SELECT * FROM ${fn}('${params[0]}'`
    : `SELECT * FROM ${fn}(${params[0]}`
  params.shift()

  return params.reduce((acc, val) => {
    if (val.isString) return `${acc}, '${val.value}'`
    if (!isNaN(val)) return `${acc}, ${val}`

    return `${acc}, '${val}'`
  }, query) + ');'
} 

const makePoolQuery = (pool) => (response, fn, params, onlyFirstRow, successCode, errorCode) => {
  pool.query(SQLQuery(fn, params), (error, result) => {
    if (error)
      return response.status(errorCode || 400).json({ error: error.message })

    if (onlyFirstRow)
      return response.status(successCode || 200).json(result.rows[0][fn] || result.rows[0])
    return response.status(successCode || 200).json(result.rows)
  })
}

const makePoolQueryAndReturn = (pool) => (fn, params, onlyFirstRow, successCode, errorCode) => {
  return new Promise ((res, rej) => {
    pool.query(SQLQuery(fn, params), (error, result) => {
      if (error) return rej(error)
      if (onlyFirstRow) res(result.rows[0][fn] || result.rows[0])
      res(result.rows)
    })
  })
}

const returnError = (response, message, code) => {
  response.status(code || 400).json({ error: message })
}

module.exports = {
  mcQuery: makePoolQuery(mc),
  maQuery: makePoolQuery(ma),
  mpQuery: makePoolQuery(mp),
  mapQuery: makePoolQuery(map),
  mpcQuery: makePoolQuery(mpc),
  mcQueryReturn: makePoolQueryAndReturn(mc),
  maQueryReturn: makePoolQueryAndReturn(ma),
  mpQueryReturn: makePoolQueryAndReturn(mp),
  returnError
}