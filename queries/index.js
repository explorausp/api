const curriculo = require('./Curriculo')
const acesso = require('./Acesso')
const pessoa = require('./Pessoa')
const acessopessoa = require('./AcessoPessoa')


module.exports = {
  ...curriculo,
  ...acesso,
  ...pessoa,
  ...acessopessoa
}
