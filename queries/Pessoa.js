const { mpQuery } = require('./helpers')


/*
 * Funções do perfil 'Visitante'
 */

const addAluno = (request, response) => {
  mpQuery(response, 'insere_aluno', request.body.values)
}

const addProfessor = (request, response) => {
  mpQuery(response, 'insere_professor', request.body.values)
}

const addAdministrador = (request, response) => {
  mpQuery(response, 'insere_administrador', request.body.values)
}

module.exports = {
  addAluno,
  addProfessor,
  addAdministrador
}